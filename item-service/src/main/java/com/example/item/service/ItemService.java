package com.example.item.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.item.client.ProductClientRest;
import com.example.item.model.Item;
import com.example.item.model.entity.Product;

@Service
public class ItemService {
	
	
	private final static Long MAGIC_NUMBER = 100L;
	
	@Autowired
	private ProductClientRest productClientRest;
	
	public List<Item> findAll() {
		List<Product> products = productClientRest.list();
		List<Item> items = products.stream().map(p -> new Item(p, MAGIC_NUMBER))
				.collect(Collectors.toList());
		return items;
	}

	public Item findById(Long id) {
		Product product = productClientRest.getProduct(id);
		return new Item(product, MAGIC_NUMBER);
	}

	public Product save(Product product) {
		return productClientRest.create(product);
	}

	public Product update(Product product, Long id) {
		return productClientRest.edit(product, id);
	}

	public void delete(Long id) {
		productClientRest.delete(id);
	}


}
