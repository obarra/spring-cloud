package com.example.product.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.product.model.entity.Product;
import com.example.product.repository.ProductRepository;

@Service
public class ProductService {
	
	private Logger LOG = LoggerFactory.getLogger(ProductService.class);
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private Environment environment;
	
	@Transactional(readOnly = true)
	public List<Product> findAll() {
		LOG.info("get list Products");
		return (List<Product>) productRepository.findAll();
	}

	@Transactional(readOnly = true)
	public Product findById(Long id){
		Product product = productRepository.findById(id).orElse(null);
		product.setPort(environment.getProperty("local.server.port"));
		return product;
	}
	
	@Transactional
	public Product save(Product product) {
		return productRepository.save(product);
	}
	
	@Transactional
	public void deleteById(Long id) {
		productRepository.deleteById(id);;
	}
	

}
